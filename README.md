# OpenML dataset: Concrete

https://www.openml.org/d/44108

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data source**

This dataset was obtained from the [UCI repository](https://archive.ics.uci.edu/ml/datasets/concrete+compressive+strength).

Original Owner and Donor
Prof. I-Cheng Yeh
Department of Information Management
Chung-Hua University,
Hsin Chu, Taiwan 30067, R.O.C.
e-mail:icyeh '@' chu.edu.tw
TEL:886-3-5186511


**Data description**

Concrete is the most important material in civil engineering. The concrete compressive strength is a highly nonlinear function of age and ingredients.


**Feature description**

* Cement (component 1) - kg in a m3 mixture
* Blast Furnace Slag (component 2) - kg in a m3 mixture
* Fly Ash (component 3) - kg in a m3 mixture
* Water (component 4) - kg in a m3 mixture
* Superplasticizer (component 5) - kg in a m3 mixture
* Coarse Aggregate (component 6) - kg in a m3 mixture
* Fine Aggregate (component 7) - kg in a m3 mixture
* Age - Day (1~365)
* Concrete compressive strength - MPa


**Relevant Papers**

Main
1. I-Cheng Yeh, "Modeling of strength of high performance concrete using artificial neural networks," Cement and Concrete Research, Vol. 28, No. 12, pp. 1797-1808 (1998).

Others
2. I-Cheng Yeh, "Modeling Concrete Strength with Augment-Neuron Networks," J. of Materials in Civil Engineering, ASCE, Vol. 10, No. 4, pp. 263-268 (1998).
3. I-Cheng Yeh, "Design of High Performance Concrete Mixture Using Neural Networks," J. of Computing in Civil Engineering, ASCE, Vol. 13, No. 1, pp. 36-42 (1999).
4. I-Cheng Yeh, "Prediction of Strength of Fly Ash and Slag Concrete By The Use of Artificial Neural Networks," Journal of the Chinese Institute of Civil and Hydraulic Engineering, Vol. 15, No. 4, pp. 659-663 (2003).
5. I-Cheng Yeh, "A mix Proportioning Methodology for Fly Ash and Slag Concrete Using Artificial Neural Networks," Chung Hua Journal of Science and Engineering, Vol. 1, No. 1, pp. 77-84 (2003).
6. Yeh, I-Cheng, "Analysis of strength of concrete using design of experiments and neural networks," Journal of Materials in Civil Engineering, ASCE, Vol.18, No.4, pp.597-604 (2006).


**Citation Request**

NOTE: Reuse of this database is unlimited with retention of copyright notice for Prof. I-Cheng Yeh and the following published paper:

I-Cheng Yeh, "Modeling of strength of high performance concrete using artificial neural networks," Cement and Concrete Research, Vol. 28, No. 12, pp. 1797-1808 (1998).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44108) of an [OpenML dataset](https://www.openml.org/d/44108). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44108/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44108/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44108/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

